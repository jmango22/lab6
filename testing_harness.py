import picosapi as picos

# define the eci for the parent node
eci = "Cnuz7WDiuvTmLxkpYTbzQJ"


# fake wovyn sensor data
def craft_temperature(temperature):
    return {"version": 2, "eventDomain": "wovyn.emitter", "eventName": "sensorHeartbeat", "emitterGUID": "5CCF7F2BD537",
            "genericThing": {"typeId": "2.1.2", "typeName": "generic.simple.temperature", "healthPercent": 56.89,
            "heartbeatSeconds": 20, "data": {"temperature": [{"name": "ambient temperature",
            "transducerGUID": "28E3A5680900008D", "units": "degrees", "temperatureF": int(temperature),
            "temperatureC": 29.69}]}}, "specificThing": {"make": "Wovyn ESProto", "model": "Temp1000",
            "typeId": "1.1.2.2.1000", "typeName": "enterprise.wovyn.esproto.wtemp.1000", "thingGUID": "5CCF7F2BD537.1",
            "firmwareVersion": "Wovyn-WTEMP1000-1.14", "transducer": [{"name": "Maxim DS18B20 Digital Thermometer",
            "transducerGUID": "28E3A5680900008D", "transducerType": "Maxim Integrated.DS18B20", "units": "degrees",
            "temperatureC": 29.69}],"battery": {"maximumVoltage": 3.6, "minimumVoltage": 2.7, "currentVoltage": 3.21}},
            "property": {"name": "Wovyn_2BD537", "description": "Temp1000", "location": {"description": "Timbuktu",
            "imageURL": "http://www.wovyn.com/assets/img/wovyn-logo-small.png", "latitude": "16.77078",
                                                                                         "longitude": "-3.00819"}}}


def create_child(name):
    url = picos.event_url(eci, "create_sensor", "sensor", "new_sensor")
    ok, response = picos.post(url, data={"section_id": name})
    # returns the Wrangler Directive to create it
    if response.content['directives'][0]['meta']['rule_name'] == 'createChild':
        print("[Success] Added pico named " + str(name))
        return True
    else:
        print("[Failed] Could not add pico named " + str(name))
        return False


def delete_child(name):
    url = picos.event_url(eci, "delete_sensor", "sensor", "unneeded_sensor")
    ok, response = picos.post(url, data={"section_id": name})
    if response.content['directives'] == []:
        print("[Failed] Could not delete pico named " + str(name))
        return False
    else:
        print("[Success] Deleted pico named " + str(name))
        return True


def child_picos():
    url = picos.event_url(eci, "sensor_list", "sensor", "get_sensors")
    ok, response = picos.get(url)
    # sends back a map of all the child picos
    return response.content['directives'][0]['options']['sensors']


def get_profile(child_eci, name):
    url = picos.event_url(child_eci, "profile", "sensor", "profile")
    ok, response = picos.get(url)
    # Check the response to check the profile is right
    if response.content['directives'][0]['options']['name'] == name:
        print("[Success] Pico profile " + name + " updated correctly")
        return True
    else:
        print("[Failed] Pico profile " + name + " not updated")
        return False


def send_temperature(child_eci, temperature, violation=False):
    url = picos.event_url(child_eci, "temperature", "wovyn", "heartbeat")
    ok, response = picos.post(url, data=craft_temperature(temperature))
    if violation:
        if response.content['directives'][0]['options']['something'] == 'NO Temperature Violation.':
            print("[Failed] Pico didn't register temperature violation")
            return False
        else:
            print("[Success] Pico registered temperature violation")
            return True
    else:
        if response.content['directives'][0]['options']['something'] == 'NO Temperature Violation.':
            print("[Success] Pico registered no temperature violation")
            return True
        else:
            print("[Failed] Pico registered temperature violation")
            return False


def test_child_creation(name_base, count):
    print("\n[TEST] Child Creation")
    for i in range(count):
        result = create_child(name_base + str(i))
        if not result:
            print("[ERROR] Failed to make " + str(count) + " child picos")
            return False
    print("[TEST][Success] Created " + str(count) + " picos")
    return True


def test_child_deletion(name_base, count):
    print("\n[TEST] Child Deletion")
    for i in range(count):
        result = delete_child(name_base + str(i))
        if not result:
            print("[ERROR] Failed to delete " + str(count) + " child picos")
            return False
    print("[TEST][Success] Deleted " + str(count) + " picos")
    return True


def test_profile_update():
    print("\n[TEST] Profile Update")
    sensor_picos = child_picos()
    for pico in sensor_picos:
        child_eci = sensor_picos[pico]['eci']
        result = get_profile(child_eci, pico)
        if not result:
            print("[ERROR] Failed to update child picos profiles")
            return False
    if sensor_picos == {}:
        print("[ERROR] No child picos!")
        return False
    else:
        print("[TEST][Success] Update child picos profiles")
        return True

def test_temperatures():
    print("\n[TEST] Temperature Tests")

    violation_temp = 71
    non_violation_temp = 69

    sensor_picos = child_picos()
    for pico in sensor_picos:
        child_eci = sensor_picos[pico]['eci']
        result = send_temperature(child_eci, violation_temp, True)
        if not result:
            print("[ERROR] Failed to register temperature event")
            return False

    for pico in sensor_picos:
        child_eci = sensor_picos[pico]['eci']
        result = send_temperature(child_eci, non_violation_temp, False)
        if not result:
            print("[ERROR] Failed to register temperature event")
            return False

    if sensor_picos == {}:
        print("[ERROR] No child picos!")
        return False
    else:
        print("[TEST][Success] Temperatures registered Correctly")
        return True


# Create Multiple sensors and delete at least one
def test1(creation, deletion):
    name_base = "Test1 "

    test_child_creation(name_base, creation)
    test_child_deletion(name_base, deletion)


# Test the sensors by ensuring they respond correctly to new temperature events.
def test2():
    test_temperatures()


# Test the sensor profile to ensure it's getting set reliably.
def test3():
    test_profile_update()


# Run the tests
test1(10, 7)
test2()
test3()
